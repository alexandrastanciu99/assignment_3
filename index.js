
const FIRST_NAME = "Alexandra";
const LAST_NAME = "Stanciu";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name,surname,salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }

     getDetails(){
        return this.name+" "+this.surname+" "+this.salary;
    } 
}

class SoftwareEngineer extends Employee{
   constructor(name,surname,salary,experience){
       super(name,surname,salary);
       if(typeof experience==="undefined"){
           experience="JUNIOR";
       }
       this.experience=experience;       
   }   

   applyBonus(){
       if(this.experience==="JUNIOR"){
           return 1.1*this.salary;
       }
       else if(this.experience==="MIDDLE"){
           return 1.15*this.salary;
       }
       else if(this.experience==="SENIOR"){
           return 1.2*this.salary;
       }
       else return 1.1*this.salary;       
   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

